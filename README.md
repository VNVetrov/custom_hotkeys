# KeyBoard Hotkeys

KeyBoard Hotkeys is a Windows program designed to function as a virtual sound card and facilitate the control of various applications' audio settings using keyboard shortcuts. It utilizes a configuration file named `current.txt`, where each line represents a new HotKey configuration.

## Usage

### Configuration Format
Each line in the `current.txt` file represents a HotKey configuration and follows the format:

```
<key combination> <operation> <arguments>
```

- `<key combination>`: Specifies the keyboard combination for the HotKey.
- `<operation>`: Defines the operation to be performed when the HotKey is triggered. Current list of operations is specified in `functs.py` file. 
  - `start_program`: Start some kinds of programs, uses Subprocess.Popen(). Requires `path` argument
  - `volume_up`: Volumes up application. Requires `process_name` and `volume_delta` arguments.
  - `volume_down`: Volumes down application. Requires `process_name` and `volume_delta` arguments.
- `<arguments>`: Additional arguments required for the operation.

### Example Configuration
```plaintext
# Example configuration

# Start Spotify
ctrl+shift+] start_program C:\Users\vladimir-pc\AppData\Roaming\Spotify\Spotify.exe

# Channel 1 - Spotify
ctrl+shift+alt+up volume_up Spotify 0.05
ctrl+shift+alt+down volume_down Spotify 0.05

# Channel 2 - Discord
ctrl+shift+alt+home volume_up Discord 0.05
ctrl+shift+alt+end volume_down Discord 0.05

# Channel 3 - Multi-application
ctrl+alt+shift+v volume_up Spotify|Discord 0.05
ctrl+alt+shift+b volume_down Spotify|Discord 0.05
```
In the above example, the last two lines demonstrate HotKeys for adjusting the volume of multiple applications simultaneously. When triggered, they will adjust the volume of both Spotify and Discord simultaneously by the specified amount.


Also you can add\edit\remove hotkey with `app.py`

```
	What would you like to do?
+--------+----------------------+
| Number |        Action        |
+========+======================+
|   1    | List current hotkeys |
+--------+----------------------+
|   2    | Add new hotkey       |
+--------+----------------------+
|   3    | Remove hotkey        |
+--------+----------------------+
1
+---------------------+---------------+--------------------------------+-------+
|   Key combination   |   Operation   |              Args              | Args  |
+=====================+===============+================================+=======+
| ctrl+shift+]        | start_program | C:\Users\vladimir-pc\AppData\R |       |
|                     |               | oaming\Spotify\Spotify.exe     |       |
+---------------------+---------------+--------------------------------+-------+
| ctrl+shift+alt+up   | volume_up     | Spotify                        | 0.050 |
+---------------------+---------------+--------------------------------+-------+
| ctrl+shift+alt+down | volume_down   | Spotify                        | 0.050 |
+---------------------+---------------+--------------------------------+-------+
| ctrl+shift+alt+home | volume_up     | Discord                        | 0.050 |
+---------------------+---------------+--------------------------------+-------+
| ctrl+shift+alt+end  | volume_down   | Discord                        | 0.050 |
+---------------------+---------------+--------------------------------+-------+

	What would you like to do?
+--------+----------------------+
| Number |        Action        |
+========+======================+
|   1    | List current hotkeys |
+--------+----------------------+
|   2    | Add new hotkey       |
+--------+----------------------+
|   3    | Remove hotkey        |
+--------+----------------------+
2
Write hotkey:

ctrl+p
Write what do you want to bind to this hotkey combination from those variants:

+---+---------------+
| 0 | volume up     |
+---+---------------+
| 1 | volume down   |
+---+---------------+
| 2 | start program |
+---+---------------+
2
Now write down the args (' ' space is a separator)
test path

	What would you like to do?
+--------+----------------------+
| Number |        Action        |
+========+======================+
|   1    | List current hotkeys |
+--------+----------------------+
|   2    | Add new hotkey       |
+--------+----------------------+
|   3    | Remove hotkey        |
+--------+----------------------+
3
Choose hotkey you want to remove
+-------+---------------------+---------------+------------------------+-------+
| Index |   Key combination   |   Operation   |          Args          | Args  |
+=======+=====================+===============+========================+=======+
| 0     | ctrl+shift+]        | start_program | C:\Users\vladimir-pc\A |       |
|       |                     |               | ppData\Roaming\Spotify |       |
|       |                     |               | \Spotify.exe           |       |
+-------+---------------------+---------------+------------------------+-------+
| 1     | ctrl+shift+alt+up   | volume_up     | Spotify                | 0.050 |
+-------+---------------------+---------------+------------------------+-------+
| 2     | ctrl+shift+alt+down | volume_down   | Spotify                | 0.050 |
+-------+---------------------+---------------+------------------------+-------+
| 3     | ctrl+shift+alt+home | volume_up     | Discord                | 0.050 |
+-------+---------------------+---------------+------------------------+-------+
| 4     | ctrl+p              | start_program | test                   | path  |
+-------+---------------------+---------------+------------------------+-------+
Choose hotkey you want to remove

4
Hotkey ctrl+p was successfully removed!

	What would you like to do?
+--------+----------------------+
| Number |        Action        |
+========+======================+
|   1    | List current hotkeys |
+--------+----------------------+
|   2    | Add new hotkey       |
+--------+----------------------+
|   3    | Remove hotkey        |
+--------+----------------------+
1
+---------------------+---------------+--------------------------------+-------+
|   Key combination   |   Operation   |              Args              | Args  |
+=====================+===============+================================+=======+
| ctrl+shift+]        | start_program | C:\Users\vladimir-pc\AppData\R |       |
|                     |               | oaming\Spotify\Spotify.exe     |       |
+---------------------+---------------+--------------------------------+-------+
| ctrl+shift+alt+up   | volume_up     | Spotify                        | 0.050 |
+---------------------+---------------+--------------------------------+-------+
| ctrl+shift+alt+down | volume_down   | Spotify                        | 0.050 |
+---------------------+---------------+--------------------------------+-------+
| ctrl+shift+alt+home | volume_up     | Discord                        | 0.050 |
+---------------------+---------------+--------------------------------+-------+
```

## Installation

To install KeyBoard Hotkeys, follow these steps:

1. Ensure you have Python installed on your system.
2. Clone this repository to your local machine.
3. Navigate to the cloned repository in your terminal or command prompt.
4. Create a virtual environment using the following command:

    ```bash
    python3 -m venv venv
    ```

5. Activate the virtual environment. The command may vary based on your operating system:

    - On Windows:
    
        ```bash
        .\venv\Scripts\activate.ps1
        ```

    - On Unix or MacOS:
    
        ```bash
        source venv/bin/activate
        ```

6. Install the required dependencies using pip:

    ```bash
    pip install -r requirements.txt
    ```

7. Build the executable using PyInstaller:

    ```bash
    pyinstaller -F daemon.py
    ```

8. After building, you will find the executable in the `/dist` directory.

9. Don't forget to create a `current.txt` file with your desired configurations for the program to function properly.

## Contributing
We welcome contributions to improve and extend the functionality of KeyBoard Hotkeys. Feel free to fork this repository and submit merge requests with your enhancements.

## Feedback
Your feedback is valuable for us to continue improving KeyBoard Hotkeys. If you encounter any issues or have suggestions for new features, please create an issue in the repository.

## License
This project is licensed under the [MIT License](LICENSE). Feel free to use and modify it according to your needs.

---

**Author**: [VNVetrov]  
**Contact**: [vladimir.vetrov28@yandex.ru]  
**Version**: 1.0.0  
**Last Updated**: [08.05.2024]