import os
import subprocess

import comtypes
from colorama import Fore
from pycaw.api.audioclient import ISimpleAudioVolume
from pycaw.utils import AudioUtilities

from volume_bar import VolumeBar

STATE = {
    'binds': {},
    'processes': set(),
    'volume_bar': VolumeBar()
}


def start_program(path):
    if not os.path.exists(path):
        return
    subprocess.Popen(path)
    print(Fore.WHITE + f"\nProgram with path {Fore.GREEN + path}{Fore.WHITE} started.")


def get_volume_instance(process_name, volume_delta):
    comtypes.CoInitializeEx(0)

    volume_delta = float(volume_delta)
    assert 1 > abs(volume_delta) > 0

    sessions = AudioUtilities.GetAllSessions()

    process_names = process_name.split('|') if '|' in process_name else [process_name]

    for session in sessions:
        for process_name in process_names:
            if session.Process and process_name.lower() in session.Process.name().lower():
                volume = session._ctl.QueryInterface(ISimpleAudioVolume)

                return volume
    return None


def volume_up(process_name, volume_delta):
    volume_delta = float(volume_delta)
    volume = get_volume_instance(process_name, volume_delta)

    if volume:
        current_volume = volume.GetMasterVolume()
        new_volume = round(min(current_volume + volume_delta, 1.0), 3)
        volume.SetMasterVolume(new_volume, None)

        volume_bar: VolumeBar = STATE['volume_bar']
        volume_bar.update(process_name, round(new_volume * 100, 2))


def volume_down(process_name, volume_delta):
    volume_delta = float(volume_delta)
    volume = get_volume_instance(process_name, volume_delta)

    if volume:
        current_volume = volume.GetMasterVolume()
        new_volume = round(max(current_volume - volume_delta, 0.0), 3)
        volume.SetMasterVolume(new_volume, None)

        volume_bar: VolumeBar = STATE['volume_bar']
        volume_bar.update(process_name, round(new_volume * 100, 2))
