from texttable import Texttable

from utils import get_current_hotkeys, initialize, FUNCTIONS, add_new_hotkey, remove_hotkey_bind


def app():
    current_hotkeys = get_current_hotkeys()
    initialize(current_hotkeys)

    space_symbols = "\n\t"

    mode_dict = {idx: func_tuple for idx, func_tuple in enumerate(FUNCTIONS.items())}

    while True:

        print(f"{space_symbols}What would you like to do?")

        action_table = Texttable()
        action_table.set_cols_align(["c", "l"])
        action_table.set_cols_valign(['c', 'c'])
        actions = [['Number', 'Action'],
                   ["1", "List current hotkeys"],
                   ["2", "Add new hotkey"],
                   ["3", "Remove hotkey"],
                   ]
        action_table.add_rows(actions)
        print(action_table.draw())

        action = input()

        if action == "1":
            current_hotkeys = get_current_hotkeys()
            current_hotkeys_table = Texttable()

            max_len = len(max(current_hotkeys, key=len))
            rows_data = []
            for hotkey in current_hotkeys:
                rows_data.append([*hotkey] + [" "] * (max_len - len(hotkey)))
            header = ["Key combination", "Operation"] + ["Args"] * (max_len - 2)
            rows_data.insert(0, header)
            current_hotkeys_table.add_rows(rows_data)

            print(current_hotkeys_table.draw())
        elif action == "2":
            print(f"{space_symbols}Write hotkey:\n")
            hotkey_comb = input()  # TODO: check comb
            print(
                f"{space_symbols}Write what do you want to bind to this hotkey"
                f" combination from those variants:\n")

            mode_variants = Texttable()
            for mode in mode_dict.items():
                mode_variants.add_row([mode[0], mode[1][0].replace("_", " ")])
            print(mode_variants.draw())
            mode = int(input())  # TODO: check mode
            operation = mode_dict[mode]
            print(f"{space_symbols}Now write down the args (' ' space is a separator)")
            args = input().split()
            add_new_hotkey(hotkey_comb, operation, args)
        elif action == "3":
            print(f"{space_symbols}Choose hotkey you want to remove")
            current_hotkeys = get_current_hotkeys()
            current_hotkeys_table = Texttable()

            max_len = len(max(current_hotkeys, key=len))
            rows_data = []
            for idx, hotkey in enumerate(current_hotkeys):
                rows_data.append([idx, *hotkey] + [" "] * (max_len - len(hotkey)))
            header = ["Index", "Key combination", "Operation"] + ["Args"] * (max_len - 2)
            rows_data.insert(0, header)

            current_hotkeys_table.add_rows(rows_data)

            print(current_hotkeys_table.draw())

            print(f"{space_symbols}Choose hotkey you want to remove\n")
            hotkey_to_remove_idx = int(input())
            hotkey_to_remove = current_hotkeys[hotkey_to_remove_idx]
            hotkey_to_remove_comb = hotkey_to_remove[0]
            remove_hotkey_bind(hotkey_to_remove_comb)
            print(f"{space_symbols}Hotkey {hotkey_to_remove_comb} was successfully removed!")
        else:
            print(f"{space_symbols}Invalid action")
            quit(-1)


if __name__ == "__main__":
    app()
