import os
from typing import Tuple, Callable

from colorama import just_fix_windows_console, Fore
from keyboard import add_hotkey, remove_hotkey

from functs import volume_up, volume_down, start_program, STATE
from volume_bar import VolumeBar

FUNCTIONS = {func.__name__: func for func in (volume_up, volume_down, start_program)}


def get_current_hotkeys():
    if os.path.exists('current.txt'):
        with open('current.txt', 'r') as f:
            current_hotkeys = f.read().splitlines()
            return [hotkey.split() for hotkey in current_hotkeys if hotkey.strip('\n') and not hotkey.startswith('#')]
    else:
        with open('current.txt', 'w') as f:
            pass
        return []


def update_state():
    for func_name_and_args in STATE['binds']:
        if func_name_and_args[1][0] in ('volume_up', 'volume_down'):
            STATE['processes'].add(func_name_and_args[1][0])
    STATE['volume_bar'] = VolumeBar(STATE['processes'])


def initialize(current_hotkeys):
    # fix colors in Windows shell
    just_fix_windows_console()

    try:
        for hotkey in current_hotkeys:
            hotkey_key, function_name, *args = hotkey
            print(
                Fore.WHITE + f"Now while pressing '{Fore.BLUE + hotkey_key}{Fore.WHITE}' you will {Fore.RED + function_name.replace('_', ' ')} {Fore.CYAN}{':'.join(args)}{Fore.WHITE}")
            if function_name in FUNCTIONS:
                function_to_call = FUNCTIONS[function_name]
                add_hotkey(hotkey_key, function_to_call, args=args)
                STATE[hotkey_key] = (function_name, args)
        update_state()
    except ValueError as e:
        print("Please be sure that you have ENG keyboard layout.", e)


def add_new_hotkey(combination, mode: Tuple[str, Callable], args):
    # do hotkey
    add_hotkey(combination, mode[1], args=args)
    # write to current.txt
    write_to_cfg(" ".join([combination, mode[0], *args]))


def write_to_cfg(line):
    with open('current.txt', 'a') as f:
        f.write("\n" + line + '\n')


def remove_hotkey_bind(combination):
    # unbind hooked combination (keyboard lib)
    remove_hotkey(combination)

    # remove hotkey from config
    with open('current.txt', 'r') as f:
        lines = f.readlines()
        for line in lines:
            if combination in line:
                lines.remove(line)
    with open('current.txt', 'w') as f:
        f.writelines(lines)
