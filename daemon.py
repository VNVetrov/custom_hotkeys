import threading

from keyboard import add_hotkey

from utils import get_current_hotkeys, initialize


def app():
    current_hotkeys = get_current_hotkeys()
    initialize(current_hotkeys)

    exit_event = threading.Event()
    add_hotkey('ctrl+shift+alt+esc', lambda x: x.set(), args=(exit_event,))
    exit_event.wait()


if __name__ == "__main__":
    app()
