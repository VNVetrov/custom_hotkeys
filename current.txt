ctrl+shift+] start_program C:\Users\vladimir-pc\AppData\Roaming\Spotify\Spotify.exe

# channel 1
ctrl+shift+alt+up volume_up Spotify 0.01
ctrl+shift+alt+down volume_down Spotify 0.01

# channel 2
ctrl+shift+alt+page_up volume_up TslGame|CS2 0.05
ctrl+shift+alt+page_down volume_down TslGame|CS2 0.05

#channel 3
ctrl+shift+alt+home volume_up Discord 0.05
ctrl+shift+alt+end volume_down Discord 0.05
ctrl+v volume_down spotify 0.01
