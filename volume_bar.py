from colorama import Fore


class VolumeBar:
    def __init__(self, processes=None):
        self.processes = processes
        self.processes_volume_dict = {}
        self.last_used_process = None

    def update(self, process_name, new_value):
        # updating
        self.processes_volume_dict[process_name.lower()] = new_value

        if self.last_used_process and self.last_used_process.lower() != process_name.lower():
            print(
                f"\r{Fore.WHITE}Volume modification switched to {Fore.GREEN}{process_name.lower().capitalize()}{Fore.WHITE}")

        bars = new_value // 5
        symbols = max(int(bars - 1), 0)
        symbols_left = 19 - symbols
        print(f"\r{Fore.GREEN}{process_name.lower().capitalize()}:{Fore.BLUE}|"
              + symbols * "="
              + ">"
              + symbols_left * '-'
              + f"| {Fore.YELLOW}{new_value}%{Fore.WHITE}", end="")

        self.last_used_process = process_name.lower()
